const fs = require('fs');
fs.readFile('.env.dist', 'utf8', (err, data) => {
  if (err) { throw err; }
  fs.writeFile('.env', data, (err, data) => {
    console.log('\x1B[32m', 'Project was successfully initialized!');
  });
});
