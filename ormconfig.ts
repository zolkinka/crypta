require('dotenv').config()
//@ts-ignore
export = {
   "type": "mysql",
   "host": "localhost",
   "port": process.env.DB_PORT,
   "username": process.env.DB_USERNAME,
   "password": process.env.DB_PASSWORD,
   "database": process.env.DB_DATABASE,
   "synchronize": false,
   "logging": false,
   "charset": "utf8mb4",
   "entities": [
      "back/entity/**/*.ts"
   ],
   "migrations": [
      "back/migration/**/*.ts"
   ],
   "subscribers": [
      "back/subscriber/**/*.ts"
   ],
   "cli": {
      "entitiesDir": "entity",
      "migrationsDir": "back/migration",
      "subscribersDir": "back/subscriber"
   }
}
