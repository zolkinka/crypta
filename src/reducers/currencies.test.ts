import {currencies} from './currencies';
import expect from 'expect';
import {defaultCurrencyState} from "../reducers/currencies";
import {SET_CURRENCY_ERROR_TRUE, SET_CURRENCY_LIST} from "../constants/actionTypes/currencies";
import {Currency} from "../models/Currency";


describe('currencies reducer', () => {

    it('returns default state when first type is incorrect', () => {
        expect(currencies(undefined, {type: '', value: ''})).toEqual(defaultCurrencyState)
    });
    it('returns correct state when type SET_CURRENCY_LIST', () => {
        expect(currencies(undefined, {type: SET_CURRENCY_LIST, value: [
            new Currency({})
            ]})).toEqual({
            ...defaultCurrencyState,
            list: [
                new Currency({})
            ]
        })
    });
    it('returns correct state when type SET_CURRENCY_ERROR_TRUE', () => {
        expect(currencies(undefined, {type: SET_CURRENCY_ERROR_TRUE, value: undefined})).toEqual({
            ...defaultCurrencyState,
            isCurrencyLoadingError: true
        })
    });

});