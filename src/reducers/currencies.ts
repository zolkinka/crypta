import {Currency} from "../models/Currency";
import {SET_CURRENCY_ERROR_TRUE, SET_CURRENCY_LIST} from "../constants/actionTypes/currencies";

export enum CurrencySortDirection {
    asc,
    desc
}
export type CurrenciesState = {
    list: Currency[];
    sortType: string;
    sortDirection: CurrencySortDirection;
    isCurrencyLoadingError: boolean;
}

export const defaultCurrencyState: CurrenciesState = {
    list: [
        new Currency({
            name: 'Goo'
        }),
        new Currency({
            name: 'Foo'
        }),
        new Currency({
            name: 'Aoo'
        }),
    ],
    sortType: 'name',
    sortDirection: CurrencySortDirection.asc,
    isCurrencyLoadingError: false,
};


export const currencies = (
    state: CurrenciesState = defaultCurrencyState,
    action: {type: string, value: any}): CurrenciesState => {
    switch (action.type) {
        case SET_CURRENCY_LIST: {
            return {
                ...state,
                list: [...action.value]
            }
        }
        case SET_CURRENCY_ERROR_TRUE: {
            return {
                ...state,
                isCurrencyLoadingError: true
            }
        }
        default:
            return state
    }
};