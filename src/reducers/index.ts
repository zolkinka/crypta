import { combineReducers } from 'redux'
import {currencies, CurrenciesState} from "./currencies";

export type GlobalState = {
    currencies: CurrenciesState
}

export default combineReducers({
    currencies,
})