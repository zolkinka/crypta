import {NO_DATA} from "../constants";
import {showBeautyNumber} from "./formatHelper";

describe('showBeautyNumber', () => {

    it('returns correct result according to precision', () => {
        expect(showBeautyNumber('1', 3)).toEqual('1.000 $');
        expect(showBeautyNumber(1.123123, 2)).toEqual('1.12 $')
        expect(showBeautyNumber('1.789', 0)).toEqual('2 $')
    });
    it('returns correct default value when first argument is incorrect', () => {
        expect(showBeautyNumber('1a.asdasd', 3)).toEqual(NO_DATA);
        expect(showBeautyNumber(undefined, 3)).toEqual(NO_DATA);
        expect(showBeautyNumber(null, 3)).toEqual(NO_DATA);
        expect(showBeautyNumber(true, 3)).toEqual(NO_DATA);
    });
});