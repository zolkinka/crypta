import {NO_DATA} from "../constants";
import numeral from 'numeral';

export function showBeautyNumber(value: any, precision: number): string {
    if(typeof value === 'string') {
        value = Number(value);
    }
    if( typeof value !== 'number' || isNaN(value) ){
        return NO_DATA
    }
    return numeral(value)
        .format(`0,0.${'0'.repeat(precision)} $`);
}