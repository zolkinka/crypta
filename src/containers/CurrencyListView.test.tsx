import React from 'react';
import {CurrencyListView, CurrencyListViewProps, CurrencyListViewState} from "./CurrencyListView";
import {Currency} from "../models/Currency";
import {shallow, ShallowWrapper} from 'enzyme';
import * as enzyme from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

enzyme.configure({adapter: new Adapter()});

describe('CurrencyListView', () => {
    let wrapper: ShallowWrapper<CurrencyListViewProps, CurrencyListViewState, CurrencyListView>;
    beforeEach(() => {
        const tableData = [
            new Currency({
                name: 'abc'
            }),
            new Currency({
                name: 'xxx'
            }),
        ];
        wrapper = shallow(
            <CurrencyListView
                tableData={tableData}
                fetchCurrencyList={() => {
                }}
                isCurrencyLoadingError={false}
            />
        )
    });

    it('renders the component', () => {
        expect(wrapper.length).toEqual(1)
    });
    it('filters tableData correctly', () => {
        wrapper.instance().setState({value: 'abc'});
        expect(wrapper.instance().filteredCurrencyList.length).toBe(1);

        // const input = wrapper.find('input.search-input')
        // expect(input.prop('value')).toEqual('');
        // input.simulate('change', {target: {value: 'newValue'}})
        // expect(wrapper.find('input.search-input').prop('value')).toEqual('newValue')
    });
});