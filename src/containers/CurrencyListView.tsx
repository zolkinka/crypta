import {connect} from 'react-redux'
import React from "react";
import {bindActionCreators} from "redux";

import './CurrencyListView.css';
import {LOADING_DATA_ERROR_MESSAGE} from "../constants";
import {fetchCurrencyList} from '../actions/currencies';
import {GlobalState} from "../reducers";
import {CurrencyTable} from "../components/CurrencyTable";
import {Currency} from "../models/Currency";

export type CurrencyListViewProps = {
    tableData: Currency[];
    isCurrencyLoadingError: boolean;
    fetchCurrencyList: () => any;
}
export type CurrencyListViewState = {
    value: string
}

export class CurrencyListView extends React.Component<CurrencyListViewProps, CurrencyListViewState> {

    constructor(props: CurrencyListViewProps) {
        super(props);
        this.state = {
            value: ''
        };
        this.handleChange = this.handleChange.bind(this);
    }

    get filteredCurrencyList() {
        const {tableData} = this.props;
        const {value} = this.state;

        if (!value) {
            return tableData;
        }
        const searchMatch = new RegExp(value, 'i');
        return this.props.tableData
            .filter(item => {
                return (
                    searchMatch.test(item.name) || searchMatch.test(item.symbol)
                )
            })
    }

    render() {
        const {
            isCurrencyLoadingError
        } = this.props;
        return (
            <div className="container">
                <div className="d-flex justify-content-between">
                    <div>
                        <h2 className="m-0">
                            CRYPTA
                        </h2>
                        <p className="m-0 subtitle">
                            Crypto Exchanger
                        </p>
                    </div>
                    <input
                        className="search-input"
                        type="text"
                        placeholder="ПОИСК"
                        value={this.state.value}
                        onChange={this.handleChange}
                    />
                </div>
                { !isCurrencyLoadingError ?
                    <CurrencyTable
                        tableData={this.filteredCurrencyList}
                    /> :
                    <div className="error-block">
                        <p>
                            {LOADING_DATA_ERROR_MESSAGE}
                        </p>
                    </div>
                }

            </div>
        );
    }

    componentDidMount(): void {
        const {fetchCurrencyList} = this.props;
        fetchCurrencyList();
    }

    handleChange(event: any) {
        this.setState({value: event.target.value});
    }
}


const mapDispatchToProps = (dispatch: any) => bindActionCreators({
    fetchCurrencyList: fetchCurrencyList
}, dispatch);
const mapStateToProps = (state: GlobalState) => ({
    tableData: state.currencies.list,
    isCurrencyLoadingError: state.currencies.isCurrencyLoadingError,
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(CurrencyListView)
