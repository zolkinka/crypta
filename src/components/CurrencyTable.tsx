import React, {Component, ReactElement} from "react";
import ReactTable from 'react-table';
import 'react-table/react-table.css';
import PropTypes, {InferProps} from 'prop-types';

import './CurrencyTable.css';
import {Currency} from "../models/Currency";
import {showBeautyNumber} from "../services/formatHelper";

export type ColItemProps = {
    value: number
}

export class CurrencyTable extends Component<InferProps<typeof CurrencyTable.propTypes>> {
    static propTypes = {
        tableData: PropTypes.arrayOf(PropTypes.instanceOf(Currency)).isRequired,
    };
    columns: {
        Header: ReactElement,
        accessor: string,
        Cell?: (props: ColItemProps) => ReactElement
    }[] = [{
        Header: <div className='string'>Имя</div>,
        accessor: 'name',
    }, {
        Header: <div className='string'>Тикер</div>,
        accessor: 'symbol',
    }, {
        Header: <div className='number'>Цена</div>,
        accessor: 'price',
        Cell: (props: ColItemProps) =>
            <div className='number'>
                {showBeautyNumber(props.value, 3)}
            </div>
    }, {
        Header: <div className='number'>Капитализация</div>,
        accessor: 'marketCap',
        Cell: (props: ColItemProps) =>
            <div className='number'>
                {showBeautyNumber(props.value, 1)}
            </div>
    }];

    render(): ReactElement {
        const {tableData} = this.props;

        return (
            <ReactTable
                columns={this.columns}
                data={tableData}
            />
        );
    }
}