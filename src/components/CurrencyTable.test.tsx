import React from 'react';
import {Currency} from "../models/Currency";
import {shallow, ShallowWrapper} from 'enzyme';
import * as enzyme from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import {CurrencyTable} from "./CurrencyTable";
import ReactTable from "react-table";
import {NO_DATA} from "../constants";

enzyme.configure({adapter: new Adapter()});

describe('CurrencyTable', () => {
    let wrapper: ShallowWrapper<typeof CurrencyTable.propTypes>;
    beforeEach(() => {
        const tableData = [
            new Currency({})
        ];
        wrapper = shallow(
            <CurrencyTable
                tableData={tableData}
            />
        )
    });

    it('renders the component', () => {
        expect(wrapper.length).toEqual(1)
    });
    it('has ReactTable component', () => {
        expect(wrapper.find(ReactTable).length).toEqual(1)
    });
});
