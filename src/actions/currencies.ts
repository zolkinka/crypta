import axios from 'axios';
import {Currency} from "../models/Currency";
import {
    SET_CURRENCY_ERROR_TRUE,
    SET_CURRENCY_LIST
} from "../constants/actionTypes/currencies";

export const setErrorTrue = () => {
    return {
        type: SET_CURRENCY_ERROR_TRUE
    };
};
export const setCurrencyList = (currencies: Currency[]) => ({
    type: SET_CURRENCY_LIST,
    value: currencies,
});

export function fetchCurrencyList() {
    return async (dispatch: any) => {
        try {
            const response = await axios.get('api/currencies');
            if (response && 'data' in  response && 'list' in response.data) {
                dispatch(setCurrencyList(response.data.list.map(
                    (item: object) => new Currency(item)
                )));
            } else {
                dispatch(setErrorTrue());
            }
        } catch (e) {
            console.error(e);
            dispatch(setErrorTrue());
        }
    }
}