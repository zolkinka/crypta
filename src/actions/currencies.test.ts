import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import moxios from 'moxios';
import expect from 'expect';

import * as actions from './currencies'
import * as types from '../constants/actionTypes/currencies'
import {Currency} from "../models/Currency";
import {defaultCurrencyState} from "../reducers/currencies";

const middlewares = [thunk];
const mockStore = configureMockStore(middlewares);

describe('setCurrencyList', () => {
    it('should create an action to set currency list', () => {
        const value = [
            new Currency({})
        ];
        const expectedAction = {
            type: types.SET_CURRENCY_LIST,
            value
        };
        expect(actions.setCurrencyList(value)).toEqual(expectedAction)
    });
});

describe('setErrorTrue', () => {
    it('should create an action to set error true', () => {
        const expectedAction = {
            type: types.SET_CURRENCY_ERROR_TRUE,
        };
        expect(actions.setErrorTrue()).toEqual(expectedAction)
    });
});

describe('getPosts actions', () => {

    beforeEach(function () {
        moxios.install();
    });

    afterEach(function () {
        moxios.uninstall();
    });

    const mockedResponse = {
        list: [
            {
                id: 1,
                name: 2
            }
        ]
    };

    it('returns correct action with correct data', () => {
        moxios.wait(() => {
            const request = moxios.requests.mostRecent();
            request.respondWith({
                status: 200,
                response: mockedResponse,
            });
        });

        const expectedActions = [
            {
                type: types.SET_CURRENCY_LIST,
                value: mockedResponse.list.map(item => new Currency(item))
            },
        ];

        const store = mockStore(defaultCurrencyState);

        // @ts-ignore
        return store.dispatch(actions.fetchCurrencyList()).then(() => {
            expect(store.getActions()).toEqual(expectedActions);
        });
    });

    it('returns error action when incorrect data', () => {
        moxios.wait(() => {
            const request = moxios.requests.mostRecent();
            request.respondWith({
                status: 200,
                response: {example: false},
            });
        });

        const expectedActions = [
            {
                type: types.SET_CURRENCY_ERROR_TRUE,
            },
        ];

        const store = mockStore(defaultCurrencyState);

        // @ts-ignore
        return store.dispatch(actions.fetchCurrencyList()).then(() => {
            expect(store.getActions()).toEqual(expectedActions);
        });
    });

    it('returns error action when get error from server', () => {
        moxios.wait(() => {
            const request = moxios.requests.mostRecent();
            request.respondWith({
                status: 500,
                response: {error: {}},
            });
        });

        const expectedActions = [
            {
                type: types.SET_CURRENCY_ERROR_TRUE,
            },
        ];

        const store = mockStore(defaultCurrencyState);

        // @ts-ignore
        return store.dispatch(actions.fetchCurrencyList()).then(() => {
            expect(store.getActions()).toEqual(expectedActions);
        });
    });
});