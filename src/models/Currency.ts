export class Currency {
    constructor(props: {[key: string]: any}) {
        this.id = props.id;
        this.serviceId = props.serviceId;
        this.name = props.name;
        this.symbol = props.symbol;
        this.price = props.price;
        this.marketCap = props.marketCap;
        this.updatedAt = props.updatedAt;
    }
    id: number;
    serviceId: number;
    name: string;
    symbol: string;
    price: number;
    marketCap: number;
    updatedAt: Date;
}