#!/usr/bin/env node
import {ParseCurrencyCommand} from "../back/commands/ParseCurrencyCommand";

switch (process.argv[2]) {
  case 'getCurrencies': {
    const command = new ParseCurrencyCommand();
    command.execute();
    break;
  }
  default: {
    console.log(`Command "${process.argv[2]}" was not found`);
  }
};