
To deploy project on the server:  
1. Run command `npm run init`  
2. Set database, password and username in .env file  
3. Install globally pm2 program (if it doesnt exist) `npm i pm2 -g`  
4. Run next commands:

`npm run build`  
`npm run migrate`  
`npm start`   

Good luck!