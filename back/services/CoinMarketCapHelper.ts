import axios from "axios";

export class CoinMarketCapHelper {
    key = '7820afe4-856d-4e4f-95e3-31c80499f19f';
    start = 1;
    limit = 100;
    convert = 'USD';
    url = 'https://pro-api.coinmarketcap.com/v1/';

    async getLatestCurrencies(): Promise<{data: object[]}> {
        const subUrl = 'cryptocurrency/listings/latest';
        return this.makeRequest(subUrl);
    }

    async makeRequest(subUrl: string) {
        const {
            key,
            start,
            limit,
            convert
        } = this;
        const result = await axios.get(this.url + subUrl, {
            params: {
                start,
                limit,
                convert,
            },
            headers: {
                'X-CMC_PRO_API_KEY': key
            },
        });
        return result.data;
    }
}