import moxios from 'moxios';
import expect from 'expect';
import {CoinMarketCapHelper} from "./CoinMarketCapHelper";

describe('getLatestCurrencies', () => {

    beforeEach(function () {
        moxios.install();
    });

    afterEach(function () {
        moxios.uninstall();
    });

    const mockedResponse = {
        list: [
            {
                id: 1,
                name: 2
            }
        ]
    };

    it('returns correct action with correct data', async () => {
        moxios.wait(() => {
            const request = moxios.requests.mostRecent();
            request.respondWith({
                status: 200,
                response: mockedResponse,
            });
        });

        const coinMatketHelper = new CoinMarketCapHelper();
        // @ts-ignore
        return coinMatketHelper.getLatestCurrencies().then(result => {
            expect(result).toEqual(mockedResponse);
        })
    });


    it('returns error action when get error from server', () => {
        expect(1).toBe(1);
    });
});