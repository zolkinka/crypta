import {Currency} from "../entity/Currency";

type CurrencyProp = {
    id: number;
    name: string;
    symbol: string;
    last_updated: string;
    quote: {
        USD: {
            price: number;
            market_cap: number;
        }
    }
}

export class CurrencyManager {
    static convertObjectToCurrency(obj: CurrencyProp) {
        const currency = new Currency();
        currency.serviceId = obj.id;
        currency.name = obj.name;
        currency.symbol = obj.symbol;
        currency.price = obj.quote.USD.price;
        currency.marketCap = obj.quote.USD.market_cap;
        currency.updatedAt = new Date(obj.last_updated);
        return currency;
    }

    static async fillCurrencyByServiceId(obj: any): Promise<Currency|undefined> {
        const serviceId = obj.id;

        if (CurrencyManager.validateObject(obj)) {
            let currency = await Currency.findOne({
                where: {
                    serviceId,
                }
            });
            if(currency) {
                currency.price = obj.quote.USD.price;
                currency.marketCap = obj.quote.USD.market_cap;
                currency.updatedAt = new Date(obj.last_updated);
            } else {
                currency = CurrencyManager.convertObjectToCurrency(obj);
            }
            return currency;
        }
    }


    static validateObject(dataItem: any): boolean {
        return dataItem instanceof Object &&
            typeof dataItem.id === 'number' &&
            typeof dataItem.name === "string" &&
            typeof dataItem.symbol === "string" &&
            typeof dataItem.last_updated === "string" &&
            'quote' in dataItem &&
            'USD' in dataItem.quote &&
            typeof dataItem.quote.USD.price === 'number' &&
            typeof dataItem.quote.USD.market_cap === "number"
    }
}