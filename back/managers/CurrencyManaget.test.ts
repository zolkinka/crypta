import expect from 'expect';
import {CurrencyManager} from "./CurrencyManager";
import {Currency} from "../entity/Currency";

const correctObject = {
    id: 1,
    name: 'str',
    symbol: 'str',
    last_updated: '2019-09-09',
    quote: {
        USD: {
            price: 123123,
            market_cap: 1231231,
        }
    }
};
const expectedCurrency = new Currency();
expectedCurrency.serviceId = 1;
expectedCurrency.name = 'str';
expectedCurrency.symbol = 'str';
expectedCurrency.updatedAt = new Date('2019-09-09');
expectedCurrency.price = 123123;
expectedCurrency.marketCap = 1231231;

describe('validateObject', () => {
    it('returns false when object is incorrect', () => {
        expect(CurrencyManager.validateObject({})).toBe(false);
        expect(CurrencyManager.validateObject(null)).toBe(false);
        expect(CurrencyManager.validateObject(undefined)).toBe(false);
        expect(CurrencyManager.validateObject(786)).toBe(false);
        expect(CurrencyManager.validateObject({
            ...correctObject,
            id: '2'
        })).toBe(false);
        expect(CurrencyManager.validateObject({
            ...correctObject,
            name: 7
        })).toBe(false);
        expect(CurrencyManager.validateObject({
            ...correctObject,
            quote: {
                USD: {
                    a: 1
                }
            }
        })).toBe(false);
    });
    it('returns true when object is correct', () => {
        expect(CurrencyManager.validateObject(correctObject)).toBe(true)
    })
});

describe('convertObjectToCurrency', () => {
    it('returns true when object is correct', () => {
        expect(CurrencyManager.convertObjectToCurrency(correctObject)).toBeInstanceOf(Currency);

        expect(CurrencyManager.convertObjectToCurrency(correctObject)).toEqual(expectedCurrency)
    });
});



describe('convertObjectToCurrency', () => {
    it('returns correct Currency when currency exists', async () => {
        Currency.findOne = jest.fn().mockResolvedValue(expectedCurrency);
        const result = await CurrencyManager.fillCurrencyByServiceId(correctObject);
        expect(result).toBeInstanceOf(Currency);

        expect(result).toEqual(expectedCurrency)
    });
    it('returns correct Currency when currency is not exists', async () => {
        Currency.findOne = jest.fn().mockResolvedValue(undefined);
        const result = await CurrencyManager.fillCurrencyByServiceId(correctObject);
        expect(result).toBeInstanceOf(Currency);

        expect(result).toEqual(expectedCurrency)
    });
    it('returns undefined when argument is incorrect', async () => {
        Currency.findOne = jest.fn().mockResolvedValue(undefined);
        const result = await CurrencyManager.fillCurrencyByServiceId({a: 1});
        expect(result).toBe(undefined)
    });
});