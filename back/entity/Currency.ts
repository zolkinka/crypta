import {
    Entity,
    PrimaryGeneratedColumn,
    Column,
    BaseEntity,
} from "typeorm";

@Entity()
export class Currency extends BaseEntity {

    @PrimaryGeneratedColumn()
    id!: number;

    @Column()
    serviceId!: number;

    @Column()
    name!: string;

    @Column()
    symbol!: string;

    @Column({type: "double"})
    price!: number;

    @Column({type: "double"})
    marketCap!: number;

    @Column()
    updatedAt!: Date;
}
