import {Mysql} from "./plugins/Mysql";
import express from 'express';
import './controllers/CurrencyController';
import {AbstractController} from './controllers/AbstractController';

const PORT = 8080;

const app = express();
// @ts-ignore
app.connection = Mysql.init();
app.use(express.urlencoded());
app.use(express.json());
app.use(express.static('build'));

AbstractController.init(app);

app.listen(PORT, () => {
    console.log(`Example app listening on port ${PORT}!`);
});
