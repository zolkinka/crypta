import {Command} from "./Command/Command";
import {Receiver} from "./Command/Receiver";
import {Mysql} from "../plugins/Mysql";
import {CoinMarketCapHelper} from "../services/CoinMarketCapHelper";
import {CurrencyManager} from "../managers/CurrencyManager";
import {Currency} from "../entity/Currency";


export class ParseCurrencyCommand implements Command {
    private output: Receiver = new Receiver();
    private timeout = 300000;
    private coinMarketCapHelper = new CoinMarketCapHelper();

    async execute() {
        await Mysql.init();
        this.run();
        setInterval(this.run.bind(this), this.timeout);
    }

    async run() {
        const res = await this.coinMarketCapHelper.getLatestCurrencies();

        if(!res.data) {
            return;
        }
        const currencyListToSave: Currency[] = [];
        for (const item of res.data) {
            const currency = await CurrencyManager.fillCurrencyByServiceId(item);
            if (currency) {
                currencyListToSave.push(currency)
            } else {
                this.output.write('Bad parsed data')
            }
        }
        try {
            await Currency.save(currencyListToSave, {chunk: 100});
            this.output.write('Currencies was successfully parsed')
        } catch (e) {
            this.output.writeError(e);
        }
    }

}