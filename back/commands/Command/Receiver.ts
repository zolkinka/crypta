export class Receiver {

    private isTimeEnabled = true;

    write(str: string) {
        if (this.isTimeEnabled) {
            const time = new Date();
            str += ` [${time.toISOString()}]`;
        }
        console.log(str);
    }

    writeError(str: string) {
        if (this.isTimeEnabled) {
            const time = new Date();
            str += ` [${time.toISOString()}]`;
        }
        console.error(str);
    }

    enableTime() {
        this.isTimeEnabled = true;
    }

    disableTime() {
        this.isTimeEnabled = false;
    }
}