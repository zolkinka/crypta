import {createConnection} from "typeorm";

export class Mysql {
    static async init() {
        return createConnection()
    }
}