import {Route} from "./base/decorators";
import {HTTP_METHOD} from "./base/consts";
import {Request, Response} from "express";
import {Currency} from "../entity/Currency";
import {AbstractController} from "./AbstractController";

export class CurrencyController extends AbstractController{
    @Route('/currencies', HTTP_METHOD.GET)
    async getList(req: Request, res: Response) {
        try {
            const list = await Currency.find(req.query);
            res.send({ list });
        } catch (e) {
            console.error(e);
            res.send(e);
        }
    }
}

