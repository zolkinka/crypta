import {API_URL, HTTP_METHOD} from "./base/consts";
import {Express} from 'express';

export class AbstractController {
    public static routes: { key: string, method: HTTP_METHOD, callback: () => void }[] = [];

    static init(app: Express) {
        const context = new this();
        AbstractController
            .routes
            .forEach(item => {
                const fullUrl = API_URL + item.key;
                const callback = item.callback.bind(context);
                app[item.method](fullUrl, callback);
            });
    }
}
