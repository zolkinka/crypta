export enum HTTP_METHOD {
    GET = 'get',
    POST = 'post',
    PUT = 'put',
    PATCH = 'patch',
    DELETE = 'delete',
    ALL = 'all'
}
export const API_URL = '/api';
