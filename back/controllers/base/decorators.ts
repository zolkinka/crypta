import {HTTP_METHOD} from "./consts";

export function Route(key: string, method: HTTP_METHOD = HTTP_METHOD.ALL) {
    return function (target: any, propertyKey: string, descriptor: PropertyDescriptor) {
        const parent = target.constructor;
        if (!parent.routes)
            parent.routes = [];
        parent.routes.push({
            key,
            method,
            callback: target[propertyKey]
        });
        return descriptor;
    }
}
