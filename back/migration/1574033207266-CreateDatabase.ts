import {MigrationInterface, QueryRunner} from "typeorm";

module.exports = class CreateDatabase1574033207266 implements MigrationInterface {
    name = 'CreateDatabase1574033207266'

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query("CREATE TABLE `currency` (`id` int NOT NULL AUTO_INCREMENT, `serviceId` int NOT NULL, `name` varchar(255) NOT NULL, `symbol` varchar(255) NOT NULL, `price` double NOT NULL, `marketCap` double NOT NULL, `updatedAt` datetime NOT NULL, PRIMARY KEY (`id`)) ENGINE=InnoDB");
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query("DROP TABLE `currency`", undefined);
    }

}
